[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 2
  ny = 2
  xmin = 0
  xmax = 200
  ymin = 0
  ymax = 200

  elem_type = QUAD4
[]

[Variables]
  [./c]
    order = FIRST
    family = LAGRANGE
    #[./InitialCondition]
    #  type = MMSCIC
    #[../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[AuxVariables]
  [./total_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  #active = 'mu_residual c_residual c_mms_forcing mu_mms_forcing'
  #active = 'mu_residual c_residual c_mms_forcing'
  #active = 'mu_residual c_residual'
  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = CH_mobility
  [../]

  [./dcdt]
    type = CoupledTimeDerivative
    variable = mu
    v = c
  [../]

  [./c_residual]
    type = BenchmarkICHSplit
    variable = c
    w = mu
    kappa_name = kappa_CH
  [../]

  [./c_mms_forcing]
     type = MMSCForcing
     variable = c
     w = 5
     kappa_CH = 2
     c_alpha = 0.3
     c_beta = 0.7
  [../]
  
  [./mu_mms_forcing]
     type = MMSMuForcing
     variable = mu 
     w = 5
     kappa_CH = 2
     c_alpha = 0.3
     c_beta = 0.7
     CH_mobility = 5
  [../]
[]

[AuxKernels]
   [./total_energy_calc]
      type = BenchmarkICHEnergy
      variable = total_energy
      CH_var = c
   [../]
[]

[Materials]
  [./generic]
    type = BenchmarkIMaterial
    CH_mobility = 5
    w = 5
    kappa_CH = 2
    c_alpha = 0.3
    c_beta = 0.7
    concentration = c
  [../]
[]

[Postprocessors]
  [./numDOFs]
    type = NumDOFs
  [../]
  [./TotalEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]
  [./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
    outputs = console
  [../]
  [./L_evals]
    type = NumResidualEvaluations
  [../]
  [./nodes]
    type = NumNodes
  [../]
  [./mms_c_error]
    type = MMSCError
    variable = c 
  [../]
  [./mms_mu_error]
    type = MMSMuError
    variable = mu 
  [../]
[]

[Executioner]
  type = Transient 

  [./TimeStepper]
    type = ImplicitEuler 
    dt = 1e-1
    cutback_factor = 0.25
    growth_factor = 1.05
    optimal_iterations = 5
    iteration_window = 1
    linear_iteration_ratio = 100
  [../]

  num_steps = 5000
  dtmin = 1e-2
  timestep_tolerance = 1e-4

  solve_type = 'NEWTON'

  l_max_its = 100
  nl_rel_tol = 1e-8
[]


[Outputs]
  exodus = true
  interval = 10
  csv = true
  file_base = MMS_CH_steady
  checkpoint = true
 sync_times = '1 5 10 20 50 100 200 500 1000 2000 3000 10000'


  [./console]
    type = Console
    perf_log = true
  [../]
[]
