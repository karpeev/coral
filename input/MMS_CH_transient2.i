[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 100 
  ny = 100 
  xmin = 0
  xmax = 1
  ymin = 0
  ymax = 1

  elem_type = QUAD4
[]

[Variables]
  [./c]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = MMSCIC
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[AuxVariables]
  [./total_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]
  [./mms_c_error_aux]
    order = FIRST
    family = LAGRANGE
  [../]
  [./mms_mu_error_aux]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  active = 'mu_residual dcdt c_residual mms_mu_forcing'
  #active = 'mu_residual dcdt c_residual'
  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = CH_mobility
  [../]

  [./dcdt]
    type = CoupledTimeDerivative
    variable = mu
    v = c
  [../]

  [./c_residual]
    type = BenchmarkICHSplit
    variable = c
    w = mu
    kappa_name = kappa_CH
  [../]

  [./mms_mu_forcing]
     type = BodyForce
     variable = mu 
     function = mms_mu_forcing_function
  [../]
[]

[Functions]
  [./mms_c_forcing_function]
    type = MMSCForcingFunction
  [../]
  [./mms_mu_forcing_function]
    type = MMSMuForcingFunction
  [../]

[]

[AuxKernels]
   [./total_energy_calc]
      type = BenchmarkICHEnergy
      variable = total_energy
      CH_var = c
   [../]
   [./mms_c_error_aux]
      type = MMSCErrorAux
      variable = mms_c_error_aux
      c = c
   [../]
   [./mms_mu_error_aux]
      type = MMSMuErrorAux
      variable = mms_mu_error_aux
      mu = mu 
   [../]
[]

[Materials]
  [./generic]
    type = BenchmarkIMaterial
    CH_mobility = 5
    w = 5
    kappa_CH = 2
    c_alpha = 0.3
    c_beta = 0.7
    concentration = c
  [../]
[]

[Postprocessors]
  [./numDOFs]
    type = NumDOFs
  [../]
  [./TotalEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]
  [./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
    outputs = console
  [../]
  [./L_evals]
    type = NumResidualEvaluations
  [../]
  [./nodes]
    type = NumNodes
  [../]
  [./mms_c_error]
    type = MMSCError
    variable = c 
  [../]
  [./mms_mu_error]
    type = MMSMuError
    variable = mu 
  [../]
[]


[Executioner]
  type = Transient

  [./TimeStepper]
    type = IterationAdaptiveDT
    dt = 1e-3
    cutback_factor = 0.25
    growth_factor = 1.05
    optimal_iterations = 5
    iteration_window = 1
    linear_iteration_ratio = 100
  [../]

  num_steps = 100
  dtmin = 1e-6
  timestep_tolerance = 1e-4

  solve_type = 'NEWTON'

  l_max_its = 100
  nl_rel_tol = 1e-6
  nl_abs_tol = 1e-11
[]


[Outputs]
  exodus = true
  csv = true
  file_base = MMS_CH_transient2

  [./console]
    type = Console
    perf_log = true
  [../]
[]
