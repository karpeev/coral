#This is an input file for Hackathon 2015 problem 2.

[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 70
  ny = 70
  xmin = -500
  xmax = 500
  ymin = -500
  ymax = 500

  elem_type = QUAD4
[]

#[GlobalParams]
#[]

[Variables]
  #phase field variable
  [./n]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = SmoothCircleIC
      invalue = 1
      outvalue = -1
      radius = 5
      int_width = 5
      x1 = 0
      y1 = 0
    [../]
  [../]

  #nondimensional temperature
  [./u]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = ConstantIC
      value = -0.3
    [../]
  [../]
[]

[AuxVariables]
  [./bulk_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]

[./gradient_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]

[./total_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]
[]

[Preconditioning]
#  [./SMP]
#    type = SMP
#    full = true
#  [../]
[]

[Kernels]
  [./dndt]
    type = TimeDerivative
    variable = n
  [../]

  [./bulk_energy]
    type = SolidificationBulk
    variable = n
  [../]

  [./int_energy_1]
    type = SolidificationInterface1
    variable = n
  [../]

  [./int_energy_2]
    type = SolidificationInterface2
    variable = n
  [../]

  [./dudt]
    type = TimeDerivative
    variable = u
  [../]

  [./diffusion]
    type = SolidificationDiffusion
    variable = u
  [../]

  [./source_term]
    type = SolidificationSourceTerm
    variable = u
    v = n
  [../]
[]

[AuxKernels]
  [./total_energy_calc]
      type = TotalEnergy
      variable = total_energy
  [../]

  [./bulk_energy_calc]
      type = BulkEnergy
      variable = bulk_energy
  [../]

  [./gradient_energy_calc]
      type = GradientEnergy
      variable = gradient_energy
  [../]
[]

[Materials]
  [./system]
    type = SolidificationMaterial

    Tm = 0 #this needs to be defined in the benchmark problem for completeness
    Cp = 1 #this needs to be defined in the benchmark problem for completeness
    latent_heat = 1 #this needs to be defined in the benchmark problem for completeness

    theta_0 = 0 #0.7853975
    m = 4
    epsilon = 0.05
    tau_0 = 1
    D = 30 #10 #30
    W_0 = 1

    temperature = u
    order_parameter = n
  [../]
[]

[BCs]
  [./temp_bc]
    type = DirichletBC
    value = -0.3
    variable = u
    boundary = '0 1 2 3'
  [../]
[]

[Postprocessors]
  [./numDOFs]
    type = NumDOFs
  [../]
  [./BulkEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = bulk_energy
  [../]
  [./GradEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = gradient_energy
  [../]
  [./TotalEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]


[./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
  [../]
  [./L_evals]
    type = NumResidualEvaluations
  [../]
  [./nodes]
    type = NumNodes
  [../]
[]

[Adaptivity]
  initial_steps = 4
  max_h_level = 4
  marker = combo
 [./Markers]
    [./EFM_1]
      type = ErrorFractionMarker
      coarsen = 0.02
      refine = 0.75
      indicator = GJI_1
    [../]
    [./EFM_2]
      type = ErrorFractionMarker
      coarsen = 0.02
      refine = 0.25
      indicator = GJI_2
    [../]
    [./combo]
      type = ComboMarker
      markers = 'EFM_1 EFM_2'
    [../]
  [../]

  [./Indicators]
    [./GJI_1]
     type = GradientJumpIndicator
     variable = n
    [../]
   [./GJI_2]
     type = GradientJumpIndicator
     variable = u
    [../]
  [../]
[]

[Executioner]
  type = Transient
#  scheme = bdf2
#  scheme = crank-nicolson
#  scheme = dirk
  scheme = rk-2

  [./TimeStepper]
    type = IterationAdaptiveDT
    dt = 1e-3
    cutback_factor = 0.75
    growth_factor = 1.05
    optimal_iterations = 3
    iteration_window = 1
    linear_iteration_ratio = 100
  [../]

  #num_steps = 5000
  end_time = 500 #3000
  #dtmin = 1e-3

  solve_type = 'PJFNK'
  petsc_options_iname = '-pc_type -sub_pc_type -sub_ksp_type -pc_asm_overlap'
  petsc_options_value = ' asm      lu           preonly       1'

  timestep_tolerance = 1e-5

  l_max_its = 100
 # nl_max_its = 10
  nl_abs_tol = 1e-11

  #trans_ss_check = 1
  #ss_check_tol = 5e-8
[]

[Outputs]
  exodus = true
  interval = 100 #20
  csv = true
  file_base = aniso4_e005_D30_NL3_RK2

  checkpoint = false #true
  sync_times = '0.1 1 5 10 50 100 250 500 750 1000 1500 2000 2500 3000'

  [./console]
    type = Console
    perf_log = true
  [../]
[]
