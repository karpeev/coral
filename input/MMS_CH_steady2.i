[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 20 
  ny = 20 
  xmin = 0
  xmax = 200
  ymin = 0
  ymax = 200

  elem_type = QUAD4
[]

[Variables]
  [./c]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = MMSCIC
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[AuxVariables]
  [./total_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  active = 'mu_residual c_residual mms_c_forcing mms_mu_forcing'
  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = CH_mobility
  [../]

  [./dcdt]
    type = CoupledTimeDerivative
    variable = mu
    v = c
  [../]

  [./c_residual]
    type = BenchmarkICHSplit
    variable = c
    w = mu
    kappa_name = kappa_CH
  [../]

  [./mms_c_forcing]
     type = BodyForce 
     function = mms_c_forcing_function
     variable = c
  [../]
  
  [./mms_mu_forcing]
     type = BodyForce
     variable = mu 
     function = mms_mu_forcing_function
  [../]
[]

[Functions]
  [./mms_c_forcing_function]
    type = MMSCForcingFunction
    CH_mobility = 5
    w = 5
    kappa_CH = 2
    c_alpha = 0.3
    c_beta = 0.7
  [../]
  [./mms_mu_forcing_function]
    type = MMSMuForcingFunction
    CH_mobility = 5
    w = 5
    kappa_CH = 2
    c_alpha = 0.3
    c_beta = 0.7
  [../]

[]

[AuxKernels]
   [./total_energy_calc]
      type = BenchmarkICHEnergy
      variable = total_energy
      CH_var = c
   [../]
[]

[Materials]
  [./generic]
    type = BenchmarkIMaterial
    CH_mobility = 5
    w = 5
    kappa_CH = 2
    c_alpha = 0.3
    c_beta = 0.7
    concentration = c
  [../]
[]

[Postprocessors]
  [./numDOFs]
    type = NumDOFs
  [../]
  [./TotalEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]
  [./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
    outputs = console
  [../]
  [./L_evals]
    type = NumResidualEvaluations
  [../]
  [./nodes]
    type = NumNodes
  [../]
  [./mms_c_error]
    type = MMSCError
    variable = c 
  [../]
  [./mms_mu_error]
    type = MMSMuError
    variable = mu 
  [../]
[]

[Executioner]
  type = Steady 

  solve_type = 'NEWTON'

  l_max_its = 100
  nl_rel_tol = 1e-8
[]


[Outputs]
  exodus = true
  csv = true
  file_base = MMS_CH_steady2

  [./console]
    type = Console
    perf_log = true
  [../]
[]
