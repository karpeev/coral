/******************************************************
 *
 *   Welcome to Coral!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   4 January 2017
 *
 *****************************************************/

#include "BulkEnergy.h"

template<>
InputParameters validParams<BulkEnergy>()
{
  InputParameters params = validParams<AuxKernel>();

  return params;
}

BulkEnergy::BulkEnergy(const InputParameters & parameters) :
    AuxKernel(parameters),
    _fbulk(getMaterialProperty<Real>("f_bulk"))
{
}

Real
BulkEnergy::computeValue()
{
  return _fbulk[_qp];

}
