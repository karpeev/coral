#include "CoralApp.h"
#include "Moose.h"
#include "AppFactory.h"
#include "ModulesApp.h"
#include "MooseSyntax.h"

//-- Coral Includes

//Benchmark I
#include "BenchmarkICHACEnergy.h"
#include "BenchmarkICHEnergy.h"
#include "BenchmarkIConcIC.h"
#include "BenchmarkIEtaIC.h"
#include "BenchmarkIACBulkPolyCoupled.h"
#include "BenchmarkICHCoupledSplit.h"
#include "BenchmarkICHSplit.h"
#include "IsotropicACBulk.h"
#include "IsotropicACInterface.h"
#include "BenchmarkIMaterial.h"

//Benchmark II
#include "SolidificationMaterial.h"
#include "SolidificationBulk.h"
#include "SolidificationInterface1.h"
#include "SolidificationInterface2.h"
#include "SolidificationSourceTerm.h"
#include "SolidificationDiffusion.h"
#include "BulkEnergy.h"
#include "GradientEnergy.h"
#include "TotalEnergy.h"

//MMS
#include "MMSCForcingFunction.h"
#include "MMSMuForcingFunction.h"
#include "MMSCIC.h"
#include "MMSCError.h"
#include "MMSMuError.h"
#include "MMSCErrorAux.h"
#include "MMSMuErrorAux.h"

// --

template<>
InputParameters validParams<CoralApp>()
{
  InputParameters params = validParams<MooseApp>();

  params.set<bool>("use_legacy_uo_initialization") = false;
  params.set<bool>("use_legacy_uo_aux_computation") = false;
  params.set<bool>("use_legacy_output_syntax") = false;

  return params;
}

CoralApp::CoralApp(InputParameters parameters) :
    MooseApp(parameters)
{
  Moose::registerObjects(_factory);
  ModulesApp::registerObjects(_factory);
  CoralApp::registerObjects(_factory);

  Moose::associateSyntax(_syntax, _action_factory);
  ModulesApp::associateSyntax(_syntax, _action_factory);
  CoralApp::associateSyntax(_syntax, _action_factory);
}

CoralApp::~CoralApp()
{
}

// External entry point for dynamic application loading
extern "C" void CoralApp__registerApps() { CoralApp::registerApps(); }
void
CoralApp::registerApps()
{
  registerApp(CoralApp);
}

// External entry point for dynamic object registration
extern "C" void CoralApp__registerObjects(Factory & factory) { CoralApp::registerObjects(factory); }
void
CoralApp::registerObjects(Factory & factory)
{
    registerMaterial(BenchmarkIMaterial);
    registerMaterial(SolidificationMaterial);

    registerKernel(BenchmarkIACBulkPolyCoupled);
    registerKernel(BenchmarkICHCoupledSplit);
    registerKernel(BenchmarkICHSplit);
    registerKernel(IsotropicACBulk);
    registerKernel(IsotropicACInterface);
    registerKernel(SolidificationBulk);
    registerKernel(SolidificationInterface1);
    registerKernel(SolidificationInterface2);
    registerKernel(SolidificationSourceTerm);
    registerKernel(SolidificationDiffusion);

    registerFunction(MMSCForcingFunction);
    registerFunction(MMSMuForcingFunction);


    registerAuxKernel(BenchmarkICHEnergy);
    registerAuxKernel(BenchmarkICHACEnergy);
    registerAuxKernel(BulkEnergy);
    registerAuxKernel(GradientEnergy);
    registerAuxKernel(TotalEnergy);
    registerAuxKernel(MMSCErrorAux);
    registerAuxKernel(MMSMuErrorAux);

    registerInitialCondition(BenchmarkIConcIC);
    registerInitialCondition(BenchmarkIEtaIC);
    registerInitialCondition(MMSCIC);

    registerPostprocessor(MMSCError);
    registerPostprocessor(MMSMuError);
}

// External entry point for dynamic syntax association
extern "C" void CoralApp__associateSyntax(Syntax & syntax, ActionFactory & action_factory) { CoralApp::associateSyntax(syntax, action_factory); }
void
CoralApp::associateSyntax(Syntax & /*syntax*/, ActionFactory & /*action_factory*/)
{
}
