/******************************************************
 *
 *   Welcome to Coral!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   7 December 2016
 *
 *****************************************************/

#ifndef SOLIDIFICATIONMATERIAL_H
#define SOLIDIFICATIONMATERIAL_H

#include "Material.h"

class SolidificationMaterial;

template<>
InputParameters validParams<SolidificationMaterial>();

class SolidificationMaterial : public Material
{
public:
  SolidificationMaterial(const InputParameters & parameters);

protected:
  virtual void computeQpProperties();
  virtual void computeA();
  virtual void computeW();
  virtual void computeTau();

  virtual void computeBulkEnergy();
  virtual void computeGradientEnergy();

private:

  //set up in constructor

  Real _Tm;
  Real _L;
  Real _Cp;
  Real _theta_0;

  int _m;

  Real _epsilon;
  Real _tau_0;
  Real _diffusion_coefficient;
  Real _W_0;

  const VariableValue & _T;
  const VariableValue & _n;

  const VariableGradient & _grad_n;

  MaterialProperty<Real> & _fbulk;
  MaterialProperty<Real> & _dfbulk_dn;
  MaterialProperty<Real> & _d2fbulk_dn2;
  MaterialProperty<Real> & _fgrad;
  MaterialProperty<Real> & _dfgrad_dn;

  MaterialProperty<RealGradient> & _dfgrad_dn_SI1;
  MaterialProperty<RealGradient> & _dfgrad_dn_SI2;

  MaterialProperty<Real> & _tau;
  MaterialProperty<Real> & _D;


  //internal variables

  Real _A;
  Real _W;

  RealGradient _dW;



};

#endif //SOLIDIFICATIONMATERIAL_H
