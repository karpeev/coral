/******************************************************
 *
 *   Welcome to Coral!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   5 January 2017
 *
 *****************************************************/

#ifndef MMSCERROR_H
#define MMSCERROR_H

#include "MMS.h"
#include "ElementIntegralVariablePostprocessor.h"

//forward declarations
class MMSCError;


template<>
InputParameters validParams<MMSCError>();

class MMSCError : public MMS, public ElementIntegralVariablePostprocessor 
{
public:
  MMSCError(const InputParameters & parameters);

protected:
  virtual Real computeQpIntegral();
};

#endif //MMSCERROR_H
