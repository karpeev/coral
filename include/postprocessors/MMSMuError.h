/******************************************************
 *
 *   Welcome to Coral!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   5 January 2017
 *
 *****************************************************/

#ifndef MMSMUERROR_H
#define MMSMUERROR_H

#include "MMS.h"
#include "ElementIntegralVariablePostprocessor.h"
#include "MaterialProperty.h"

//forward declarations
class MMSMuError;


template<>
InputParameters validParams<MMSMuError>();

class MMSMuError : public MMS, public ElementIntegralVariablePostprocessor 
{
public:
  MMSMuError(const InputParameters & parameters);

protected:
  
  virtual Real computeQpIntegral();
};

#endif //MMSMUERROR_H
