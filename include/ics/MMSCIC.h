/******************************************************
 *
 *   Welcome to Hackathon!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   19 February 2016
 *
 *****************************************************/

#ifndef MMSCIC_H
#define MMSCIC_H

#include "MMS.h"
#include "InitialCondition.h"

// Forward Declarations
class MMSCIC;
namespace libMesh { class Point; }

template<>
InputParameters validParams<MMSCIC>();

class MMSCIC : public MMS, public InitialCondition
{
public:
  MMSCIC(const InputParameters & parameters);
  virtual Real value(const Point & p);

protected:
  Real _epsilon;
};

#endif //MMSCIC_H
