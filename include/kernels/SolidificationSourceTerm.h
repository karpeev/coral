/******************************************************
 *
 *   Welcome to Coral!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   8 December 2016
 *
 *****************************************************/

#ifndef SOLIDIFICATIONSOURCETERM_H
#define SOLIDIFICATIONSOURCETERM_H

#include "CoupledTimeDerivative.h"

class SolidificationSourceTerm;

template<>
InputParameters validParams<SolidificationSourceTerm>();

class SolidificationSourceTerm : public CoupledTimeDerivative
{
public:
  SolidificationSourceTerm(const InputParameters & parameters);

  virtual Real computeQpResidual();
  //virtual Real computeQpJacobian();
  virtual Real computeQpOffDiagJacobian(unsigned int jvar);


protected:

private:
  //const MaterialProperty<Real> & _dfdn;
  //const MaterialProperty<RealGradient> & _dfgrad_dn_SI1;
  //const MaterialProperty<RealGradient> & _dfgrad_dn_SI2;
  //const MaterialProperty<Real> & _tau;

};


#endif //SOLIDIFICATIONSOURCETERM_H
