/*************************************************************************
*
*  Welcome to Hackathon!
*  Andrea M. Jokisaari
*
*  22 February 2016
*
*************************************************************************/

#ifndef BENCHMARKICHSPLIT_H
#define BENCHMARKICHSPLIT_H

#include "SplitCHCRes.h"

//forward declarations
class BenchmarkICHSplit;

template<>
InputParameters validParams<BenchmarkICHSplit>();

class BenchmarkICHSplit : public SplitCHCRes
{
public:
  BenchmarkICHSplit(const InputParameters & parameters);

protected:
  virtual Real computeDFDC(PFFunctionType type);
//virtual Real computeDEDC(PFFunctionType type);

  const MaterialProperty<Real> & _c_alpha;
  const MaterialProperty<Real> & _c_beta;
  const MaterialProperty<Real> & _w;

private:

};

#endif //BENCHMARKICHSPLIT_H
