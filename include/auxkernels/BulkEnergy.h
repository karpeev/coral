/******************************************************
 *
 *   Welcome to Coral!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   4 January 2017
 *
 *****************************************************/

#ifndef BULKENERGY_H
#define BULKENERGY_H

#include "AuxKernel.h"

//forward declarations
class BulkEnergy;


template<>
InputParameters validParams<BulkEnergy>();

class BulkEnergy : public AuxKernel
{
public:
  BulkEnergy(const InputParameters & parameters);

protected:
  virtual Real computeValue();

private:
  const MaterialProperty<Real> & _fbulk;
};

#endif //BULKENERGY_H
