/*************************************************************************
*
*  Welcome to Hackathon!
*  Andrea M. Jokisaari
*
*  22 February 2016
*
*************************************************************************/
#ifndef MMSMUERRORAUX_H
#define MMSMUERRORAUX_H
#include "AuxKernel.h"
#include "MMS.h"

//Forward Declarations
class MMSMuErrorAux;

template<>
InputParameters validParams<MMSMuErrorAux>();

class MMSMuErrorAux : public AuxKernel, public MMS
{
public:
  MMSMuErrorAux(const InputParameters & parameters);

protected:
  virtual Real computeValue();

protected:
  const VariableValue & _mu;
};

#endif //MMSMUERRORAUX
