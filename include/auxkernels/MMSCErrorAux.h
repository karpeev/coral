/*************************************************************************
*
*  Welcome to Hackathon!
*  Andrea M. Jokisaari
*
*  22 February 2016
*
*************************************************************************/
#ifndef MMSCERRORAUX_H
#define MMSCERRORAUX_H
#include "AuxKernel.h"
#include "MMS.h"

//Forward Declarations
class MMSCErrorAux;

template<>
InputParameters validParams<MMSCErrorAux>();

class MMSCErrorAux : public AuxKernel, public MMS
{
public:
  MMSCErrorAux(const InputParameters & parameters);

protected:
  virtual Real computeValue();

protected:
  const VariableValue & _c;
};

#endif //MMSCERRORAUX
