/******************************************************
 *
 *   Welcome to Coral!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   5 January 2017
 *
 *****************************************************/

#ifndef TOTALENERGY_H
#define TOTALENERGY_H

#include "AuxKernel.h"

//forward declarations
class TotalEnergy;


template<>
InputParameters validParams<TotalEnergy>();

class TotalEnergy : public AuxKernel
{
public:
  TotalEnergy(const InputParameters & parameters);

protected:
  virtual Real computeValue();

private:
  const MaterialProperty<Real> & _fbulk;
  const MaterialProperty<Real> & _fgrad;

//  const MaterialProperty<Real> & _fbulk;
};

#endif //TOTALENERGY_H
