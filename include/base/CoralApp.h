#ifndef CORALAPP_H
#define CORALAPP_H

#include "MooseApp.h"

class CoralApp;

template<>
InputParameters validParams<CoralApp>();

class CoralApp : public MooseApp
{
public:
  CoralApp(InputParameters parameters);
  virtual ~CoralApp();

  static void registerApps();
  static void registerObjects(Factory & factory);
  static void associateSyntax(Syntax & syntax, ActionFactory & action_factory);
};

#endif /* CORALAPP_H */
