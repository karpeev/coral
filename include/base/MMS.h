#ifndef MMS_H
#define MMS_H
#include "Moose.h"
#include "InputParameters.h"

class MMS
{
public: 
  const Real _A = 1e-0; 
  const Real _kx = 1.0;/**2.0*libMesh::pi;*/
  const Real _ky = 1.0;/**2.0*libMesh::pi;*/
  const Real _omega = 0.0;

  const Real  _M = 5.0;
  const Real  _kappa = 2.0;
  const Real  _c_alpha = 0.3;
  const Real  _c_beta = 0.7;
  const Real  _w = 5.0;
 
  Real computeC(Real x, Real y, Real t);
  Real computeDfDc(Real x, Real y, Real t);
  Real computeLaplacianC(Real x, Real y, Real t);

  Point computeGradC(Real x, Real y, Real t);
  Real computeLaplacian2C(Real x, Real y, Real t);
  Real computeD2fDc2(Real x, Real y, Real t);
  Real computeD3fDc3(Real x, Real y, Real t);

};

#endif /* MMS_H */
