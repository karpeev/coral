/******************************************************
 *
 *   Welcome to Coral!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   5 January 2017
 *
 *****************************************************/

#ifndef MMSCFORCINGFUNCTION_H
#define MMSCFORCINGFUNCTION_H

#include "MMS.h"
#include "Function.h"

//forward declarations
class MMSCForcingFunction;


template<>
InputParameters validParams<MMSCForcingFunction>();

class MMSCForcingFunction : public MMS, public Function
{
public:
  MMSCForcingFunction(const InputParameters & parameters);

protected:
  
  virtual Real value(Real t, const Point& p);
  
  Real computeForcing(Real x, Real y, Real t);
};

#endif //MMSCFORCINGFUNCTION_H
