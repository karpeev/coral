/******************************************************
 *
 *   Welcome to Coral!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   5 January 2017
 *
 *****************************************************/

#ifndef MMSMUFORCINGFUNCTION_H
#define MMSMUFORCINGFUNCTION_H

#include "MMSCForcingFunction.h"

//forward declarations
class MMSMuForcingFunction;


template<>
InputParameters validParams<MMSMuForcingFunction>();

class MMSMuForcingFunction : public MMSCForcingFunction 
{
public:
  MMSMuForcingFunction(const InputParameters & parameters);

protected:

  
  virtual Real value(Real t, const Point& p);

  Real computeForcing(Real x, Real y, Real t);
};

#endif //MMSMUFORCINGFUNCTION_H
